,===
"Database ID","RNA Secondary Structure (Binding Location)","size of loop",sequence
"R-BIND (SM) 0002",-,,
"R-BIND (SM) 0004",-,,
"R-BIND (SM) 0046",Stem-Loop,,
"R-BIND (SM) 0021",Bulge,1,A
"R-BIND (SM) 0022A",Bulge,1,A
"R-BIND (SM) 0027A","Internal Loop","1,1","G,G"
"R-BIND (SM) 0032","Internal Loop",,
"R-BIND (SM) 0039",-,,
"R-BIND (SM) 0040",Bulge,5,AACUA
"R-BIND (SM) 0041",Bulge,5,AACUA
"R-BIND (SM) 0042",Bulge,5,AACUA
"R-BIND (SM) 0043",Bulge,5,AACUA
"R-BIND (SM) 0044",Bulge,5,AACUA
"R-BIND (SM) 0047",-,,
"R-BIND (SM) 0048",-,,
"R-BIND (SM) 0049",Bulge*,3,UCU
"R-BIND (SM) 0058",Bulge,3,UCU
"R-BIND (SM) 0059",Bulge,3,
"R-BIND (SM) 0061",Bulge,1,A
"R-BIND (SM) 0062",Bulge,1,A
"R-BIND (SM) 0063",Bulge,1,A
"R-BIND (SM) 0064",Bulge,1,A
"R-BIND (SM) 0065",Bulge,1,A
"R-BIND (SM) 0066",-,,
"R-BIND (SM) 0069",Bulge,1,
"R-BIND (SM) 0070","Internal Loop",1,
"R-BIND (SM) 0071",Bulge,,
"R-BIND (SM) 0072","Apical Loop",4,GGAG
"R-BIND (SM) 0073",-,,
"R-BIND (SM) 0074",-,,
"R-BIND (SM) 0075",-,,
"R-BIND (SM) 0081","Internal Loop",,
"R-BIND (SM) 0082","Internal Loop",,
"R-BIND (SM) 0086","Internal Loop",,
"R-BIND (SM) 0091",Stem-Loop,,
"R-BIND (SM) 0092","Double Stranded RNA (dsRNA) and ESE2 Motif",,
"R-BIND (SM) 0093","Double Stranded RNA (dsRNA)",,
"R-BIND (SM) 0094","Double Stranded RNA (dsRNA)",,
"R-BIND (SM) 0095","Double Stranded RNA (dsRNA) and ESE2 Motif",,
"R-BIND (SM) test","Apical Loop",9,
"R-BIND (SM) test1","Apical Loop",8,
"R-BIND (SM) test2","Internal Loop","2,4",
"R-BIND (SM) test3","Internal Loop","3,3;4,6",
,===
