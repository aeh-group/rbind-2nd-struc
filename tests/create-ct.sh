#echo "please input the RNA sequence you are interested"
#read sequence
#sequence = "GGUGCUGUUAUCCAUUUCAGAAUUGGGUGUCGACAUAGCACC"
#echo "Do you have 2nd structure information? Yes type 1, No type 2"
#read i
i=2
sequence=$1
if (( $i==1 ));
then
  echo "please input dot and bracket notation for it's secondary structure?"
  read dotandbracket
  echo "$sequence" >> sequence.txt
  echo "$dotandbracket" >> sequence.txt
  ## calculate the energy for 2nd structure ##
  RNAeval sequence.txt > sequence2.txt
  ##check whether length of sequence = length of dotandbracket ##
  output1=$(awk 'NR==1 { print length }' 'sequence.txt')
  output2=$(awk 'NR==2 { print length }' 'sequence.txt')
    if (( $output1==$output2 ));
    then
#      cc -o b2ct b2ct.c
      b2ct < sequence2.txt > ct_file.ct

      sed 1d ct_file.ct > ct_file_tm.ct
      awk '{print $1, $5}' ct_file_tm.ct | column -t > connect.ct

      sleep 1

#      python RNA2ndStructure_searchingSM.py
      ./RNA2ndStructure_searchingSM.py
#      rm sequence.txt
#      rm sequence2.txt
    else
      echo "The length of input sequence doesn't match with length of dotandbracket"
#      rm sequence.txt
#      rm sequence2.txt
    fi
else
  echo "$sequence" > test.ct
  #cat test.txt | RNAfold -p > test1.txt
  ##  save it as connecting file format ##
#  RNAfold < test.ct | b2ct > ct_file.ct
  RNAfold < test.ct | b2ct > ct_file.ct

  #head -2 test1.txt | tail -1 |cut -d ' ' -f 1 > test2.txt

  ##  remove first row of file energy term ##
  sed 1d ct_file.ct > ct_file_tm.ct
  awk '{print $1, $5}' ct_file_tm.ct | column -t > connect.ct

  sleep 1

#  python RNA2ndStructure_searchingSM.py
#   echo "$sequence"
   RNAfold < test.ct
   python  ./RBIND_2ndS.py
#  rm test.ct
fi
rm rna.ps 2> /dev/null
rm *.ct   2> /dev/null
