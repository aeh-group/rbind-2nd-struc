## rbind-2nd-struc (What is this project?)
This project provides a new feature (and API) for searching [RBIND](https://gitlab.oit.duke.edu/aeh-group/db-website/-/issues/42) small molecules through RNA 2nd structural shape. 

## Motivation (Why?)
TODO: intended audience, etc.

## Build status
Build status of continus integration i.e. travis, appveyor etc. Ex. - 

[![Build Status](https://travis-ci.org/akashnimare/foco.svg?branch=master)](https://travis-ci.org/akashnimare/foco)
[![Windows Build Status](https://ci.appveyor.com/api/projects/status/github/akashnimare/foco?branch=master&svg=true)](https://ci.appveyor.com/project/akashnimare/foco/branch/master)

## Code style
python, pytest, pydoc

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](https://github.com/feross/standard)
 
## Screenshots, Figures, etc
TODO: Add caption to explain concept.
<p align="center">
  <img src="./img/concept.png" alt="concept.png" width="738">
</p>

## Tech/framework used

<b>Built with</b>
- [gitlab ci/cd](https://docs.gitlab.com/ee/ci/README.html)

## Features
Initially, we are thinking of providing a web-interface and API similar to [forna](http://nibiru.tbi.univie.ac.at/forna/forna.html?id=RNAcentral/URS0000000001)
The source code for above forna website is hosted at [https://github.com/ViennaRNA/forna](https://github.com/ViennaRNA/forna)

## Demo Example (How)
```command
$ conda activate py3vienna  ### see below on how to do one-time-only setup of py3vienna env
(py3vienna)$ ./rbind2.sh
please input the RNA sequence you are interested
CGGAGGAACUACUGUCUUCACGCGCGUGUCGUGCAGCCUCCG
Do you have 2nd structure information? Yes type 1, No type 2
please input dot and bracket notation for it's secondary structure?
# control-c to exit. (What is example of "input dot and bracket" needed to run demo?
$ ./i.sh
please input the RNA sequence you are interested
CGGAGGAACUACUGUCUUCACGCGCGUGUCGUGCAGCCUCCG
Do you have 2nd structure information? Yes type 1, No type 2
2
There is(are) 1 apical loop(s)
There is(are) 2 bulge loop(s)
There is(are) 2 internal loop(s)
whats size of apical loop [4]
whats size of bulge loop [5, 1]
whats size of internal loop [2, 2]
         Database ID  ... sequence
43  R-BIND (SM) 0142  ...    AACUA
44  R-BIND (SM) 0143  ...    AACUA
45  R-BIND (SM) 0144  ...    AACUA
46  R-BIND (SM) 0145  ...    AACUA

[4 rows x 4 columns]
          Database ID  ... sequence
3    R-BIND (SM) 0021  ...        A
4   R-BIND (SM) 0022A  ...        A
18   R-BIND (SM) 0061  ...        A
19   R-BIND (SM) 0062  ...        A
20   R-BIND (SM) 0063  ...        A
21   R-BIND (SM) 0064  ...        A
22   R-BIND (SM) 0065  ...        A
24   R-BIND (SM) 0069  ...      NaN
25   R-BIND (SM) 0070  ...      NaN

[9 rows x 4 columns]
         Database ID  ... sequence
27  R-BIND (SM) 0072  ...     GGAG

[1 rows x 4 columns]
$
```

## Installation

Pre-requisites
  * Python3  pandas xlrd nose. We suggest using [minconda3 and bioconda](https://bioconda.github.io/user/install.html)
  * RNAfold from [ViennaRNA-2.4.14](https://github.com/ViennaRNA/ViennaRNA).  We found bioconda was easiest way to install regularly updated viennarna, such as v2.4.14 currently)

One-time-only, we can install above pre-requisites and create py3vienna env using steps below.

First install [miniconda3]((https://bioconda.github.io/user/install.html). If you have brew: `brew install miniconda3

Then install viennrna with commands similar to the following (on windows it would be just `activate py3rna`)
``` command
$ conda deactivate
$ conda create --name py3vienna python=3.8 # or maybe just python=3 if issues
$ conda config --set auto_activate_base false
$ conda activate py3vienna
(py3vienna) $ conda config --add channels defaults
(py3vienna) $ conda config --add channels bioconda
(py3vienna) $ conda config --add channels conda-forge
(py3vienna) $ conda install viennarna nose xlrd pandas
(py3vienna) $ RNAfold --version ### verifies viennarna suite is installed

```
TODO: Add further steps to install rbind2.sh etc. Maybe provide for each platform (win,mac,linux) an environment.yml for conda?

## API Reference

TODO: Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. For medium size to larger projects it is important to at least provide a link to where the API reference docs live.

## Tests
TODO: Describe and show how to run the tests with code examples.

## How to use?
TODO: include step by step guide to use your project.

## Contribute

Let people know how they can contribute into your project beyond just creating project-issues. A [contributing guideline](https://github.com/zulip/zulip-electron/blob/master/CONTRIBUTING.md) will be a big plus.

## Credits
Liang-Yuan Chiu
Andrew Sugarman, Emily Swanson Tolbert lab, Amanda lab

#### Anything else that seems useful

## License
TODO: A short snippet describing the license (MIT, Apache etc)

MIT © [Liang-Yuan Chiu]()